<h2>Liste des factures ayant reçues un règlement ce mois-ci</h2>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Numéro</th>
            <th>Date</th>
            <th>Client</th>
            <th>Montant TTC</th>
            <th>Règlements sur le mois</th>
            <th>Montant réglé ce mois</th>
            <th>Date du dernier règlement</th>
            <th>Commentaire</th>
        </tr>
    </thead>
    <tbody>
<?php

        
$currentData = $data['invoicesWithPaymentsThisMonth'];
        

        
foreach($currentData['list'] as $item) {
    
    extract($item['invoice']);

?>
        <tr>
            <td><a href="https://www.sellsy.fr/?_f=invoiceOverview&id={{ $id }}" target="_blank">{{ $ident }}</a></td>
            <td>{{ $formatted_created }}</td>
            <td>{{ $thirdname }}</td>
            <td style="text-align:right;">{{ format_number_id($totalAmount) }}</td>
            <td>
                <table class="table table-striped" style="font-size:10px;">
<?php
                
    $total = 0;
    foreach($item['paymentsTargeted'] as $p) {
        echo '<tr><td>'.$p['formatted_date'].'</td><td>'.format_number_id($p['amount']).'</td></tr>';
        
        $total += $p['amount'];
    }
?>
                </table>
            </td>
            <td style="text-align:right;">{{ format_number_id($total) }}</td>
            <td>{{ $formatted_lastpayment }}</td>
            <td>
            </td>
        </tr>

<?php
}
?>
    </tbody>
</table>

<h3>Total réglé ce mois-ci : {{ format_number_id($data['invoicesWithPaymentsThisMonth']['total']) }}</h3>

<hr />

<h2>Liste des règlements</h2>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Réconciliation</th>
            <th>Date</th>
            <th>Désignation</th>
            <th>Type</th>
            <th>Montant</th>
            <th>Commentaire</th>
        </tr>
    </thead>
    <tbody>
<?php

        
$currentData = $data['paymentsThisMonth'];
        
$total_client_reconcilie = 0;
$total_non_reconcilie = 0;
$total_externe = 0;
$total = 0;
foreach($currentData['list'] as $item) {
    
    extract($item);
    
    if ($item['reconciliationStatus']['code'] != 'reconciled') {
        $total_non_reconcilie += $amount;
    }
    if ($item['linkedRelationType'] == 'client' && $item['reconciliationStatus']['code'] == 'reconciled') {
        $total_client_reconcilie += $amount;
    }
        
    if ($item['linkedRelationType'] != 'client') {
        $total_externe += $amount;
    }
    
    $total += $amount;
    
    

?>
        <tr>
            <td>
                @if ($item['reconciliationStatus']['code'] == 'reconciled')
                <i class="fas fa-check-square" style="color:#078037"></i>
                @else
                <i class="fas fa-times-circle" style="color:#d03b00"></i>
                @endif
            </td>
            <td>{{ $dateFormatted }}</td>
            <td>{{ $linked }}</td>
            <td>
                @if ($item['linkedRelationType'] == 'client')
                <span class="badge badge-success">client</span>
                @else
                <span class="badge badge-danger">externe</span>
                @endif
            </td>
            <td style="text-align:right;">{{ format_number_id($amount) }}</td>
            <td>
            </td>
        </tr>

<?php
}
?>
    </tbody>
</table>
<table class="table table-striped float-right" style="max-width: 400px;">
    <tr>
        <td>Total client réconcilié</td><td style="text-align:right;">{{ format_number_id($total_client_reconcilie) }}</td>
    </tr>
    <tr>
        <td>Total non réconcilié</td><td style="text-align:right;">{{ format_number_id($total_non_reconcilie) }}</td>
    </tr>
    <tr>
        <td>Total externe</td><td style="text-align:right;">{{ format_number_id($total_externe) }}</td>
    </tr>
    <tr>
        <td>Total</td><td style="text-align:right;">{{ format_number_id($total) }}</td>
    </tr>
</table>