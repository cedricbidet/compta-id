@extends('layouts.app')


@section('content')

<!-- Global stats -->
<div class="row">
    <table class="table monetaire">
        <thead>
            <tr>
                <td></td>
<?php
foreach($months_available as $month) {
    echo '<th>' . $month['label'] . '</th>';
}
?>
            </tr>
        </thead>
        <tbody>
            <tr style="background:#078037; color:white;">
                <td style="text-align:left;">C.A.</td>
<?php
foreach($global_stats['ca'] as $month => $ca) {
    echo '<td>' . format_number_id($ca) . '</td>';
}

echo do_td_from_n_to_12(count($global_stats['ca']));

?>
            </tr>
            <tr style="background:#fb6800; color:white;">
                <td style="text-align:left;">Charges</td>
<?php
foreach($global_stats['chargeDetails'] as $month => $charges) {
    echo '<td>' . format_number_id($charges['total']) . '</td>';
}
echo do_td_from_n_to_12(count($global_stats['chargeDetails']));
?>
            </tr>
<?php

foreach($purchase_types as $type => $type_label) {
?>
            <tr style="background:#ffa96d; color:black;">
                <td><?php echo $type_label; ?></td>
    <?php
    foreach($global_stats['chargeDetails'] as $month => $charges) {
        if (isset($charges['details'][$type])) {
            echo '<td>' . format_number_id($charges['details'][$type]) . '</td>';
        } else {
            echo '<td>' . format_number_id(0) . '</td>';
        }
    }
    echo do_td_from_n_to_12(count($global_stats['chargeDetails']));
    ?>
            </tr>
<?php
}
?>
        </tbody>
    </table>
</div>
<!-- /Global stats -->

<div class="row">
    <nav class="nav nav-pills nav-fill mb-3" id="months-tabs" role="tablist">
        <?php
        foreach($months_available as $m_index => $m_data) {
            $active = $current_year == $m_data['year'] && $current_month == $m_data['month'];
            echo '  <a href="#panel-'.$m_data['month'].'-'.$m_data['year'].'"
                        id="tab-'.$m_data['month'].'-'.$m_data['year'].'"
                        class="nav-item nav-link ' . ($active ? 'active' : '') . '"
                        data-month="' . $m_data['month'] . '"
                        data-year="' . $m_data['year'] . '"
                        aria-controls="panel-'.$m_data['month'].'-'.$m_data['year'].'"
                        aria-selected="'.($active ? 'true' : 'false').'"
                        data-toggle="tab"
                        role="tab"
                    >' . $m_data['label'] . '</a>';
        }
        ?>
    </nav>

    <div class="tab-content" id="months-tabs-content">

        <?php
        foreach($months_available as $m_index => $m_data) {
            echo '<div
                        class="tab-pane fade show ' . ($current_year == $m_data['year'] && $current_month == $m_data['month'] ? 'active' : '') . '"
                        id="panel-'.$m_data['month'].'-'.$m_data['year'].'"
                        role="tabpanel"
                        aria-labelledby="tab-'.$m_data['month'].'-'.$m_data['year'].'">';
            ?>

            @isset ($datas[$m_data['year']][$m_data['month']])
            @include('dashboard.month', ['month' => $m_data['month'], 'year' => $m_data['year'], 'data' => $datas[$m_data['year']][$m_data['month']]])
            @endisset

            <?php
            echo '</div>';
        }
        ?>
    </div>
</div>

@endsection

