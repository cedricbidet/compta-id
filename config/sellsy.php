<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Driver
    |--------------------------------------------------------------------------
    |
    | Configurer ici le driver pour la connexion Sellsy. Choix possibles :
    | - laisser vide : utiliser l'API Sellsy (nécessite connexion réseau + identifiants API)
    | - fake : utiliser les classes de mock pour simuler les retours API Sellsy (hors connexion / testing)
    |
    */

    'driver' => env('SELLSY_DRIVER', ''),

    /*
    |--------------------------------------------------------------------------
    | Authentification
    |--------------------------------------------------------------------------
    |
    | Identifiants API
    |
    */

    'auth' => [
        'consumer_token' => env('SELLSY_CONSUMER_TOKEN', ''),
        'consumer_secret' => env('SELLSY_CONSUMER_SECRET', ''),
        'user_token' => env('SELLSY_USER_TOKEN', ''),
        'user_secret' => env('SELLSY_USER_SECRET', ''),
    ],

    'steps' => [
        'creditnote' => [
            'brouillon' => 'draft',
            'asolder' => 'stored',
            'partiel' => 'partielspend',
            'solde' => 'spent',
            'annule' => 'cancelled'
        ]
    ],

    'default_payment_type_id' => env('SELLSY_DEFAULT_PAYMENT_TYPE_ID', 3235938),

];
