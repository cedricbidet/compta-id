<?php

if (! function_exists('format_number_id')) {
    function format_number_id($number)
    {
        return number_format($number, 2, ',', '').'€';
    }
}

if (! function_exists('do_td_from_n_to_12')) {
    function do_td_from_n_to_12($n) {
        $s = '';
        for($i = $n; $i < 12; $i ++) {
            $s .= '<td></td>';
        }
        return $s;
    }
}