<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SellsyID;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DashboardController extends Controller {
    
    /* Le début de l'exercice est au au mois d'octbre jusqu'au au mois de sepemtre */
    const START_MONTH = 10;
    const END_MONTH = 9;
    const START_YEAR = 2019;
    const END_YEAR = 2020;
    
    private $sellsy;
    
    public function showStats(Request $request) {
        
        $this->initSellsy();
        
        
        /*
        On ne charge qu'un exercice pour le moment. Il commence au 01/10 et se termine au 30/09
        */
        
        $request->validate([
            'current_month' => 'between:1,12',
        ]);
        
        
        
        $now = Carbon::now();
        $current_year = $now->year;
        
        /* liste des tabs par mois : on commence par octobre */
        $months = [
            10 => '10',
            11 => '11',
            12 => '12',
            1 => '01',
            2 => '02',
            3 => '03',
            4 => '04',
            5 => '05',
            6 => '06',
            7 => '07',
            8 => '08',
            9 => '09'
        ];

        
        /**
        Construction des tabs :
        - le label est de la forme mm/yyyy
        - le mois au format numérique m (donc "1" pour janvier)
        - l'année au format numérique long yyyy
        **/
        
        $months_available = [];
        $tmp_year = self::START_YEAR;
        foreach($months as $m_index => $m_label) {
            $months_available[] = [
                'label' => $m_label.'/'.$tmp_year,
                'month' => $m_index,
                'year' => $tmp_year
            ];
            
            if ($m_index == 12) {
                $tmp_year ++;
            }
        }
        
        
        // Par défaut, on affiche les données du mois en cours
        if ($request->input('current_month')) {
            $current_month = $request->input('current_month');
        } else {
            $current_month = $now->month;
        }
        
        if ($request->input('current_year')) {
            $current_year = $request->input('current_year');
        } else {
            $current_year = $now->year;
        }
        
        
        // Chargement des données globales
        $startDate = Carbon::create(self::START_YEAR, self::START_MONTH, 1);
        $endDate = Carbon::create(self::END_YEAR, self::END_MONTH, 1)->endOfMonth();
        
        $global_stats = $this->sellsy->getGlobalStats(['ca', 'chargeDetails'], $startDate->toMutable(), $endDate->toMutable());
        
        // On va chercher les datas pour chaque mois jusqu'au maximum (= à la fin de l'exercice ou aujourd'hui)
        $tempEndDate = $now->lessThan($endDate) ? $now : $endDate;
        
        $datas = [];
        while($startDate->lessThanOrEqualTo($tempEndDate)) {
            
            if (!isset($datas[$startDate->year])) {
                $datas[$startDate->year] = [];
            }
            
            $datas[$startDate->year][$startDate->month] = $this->getStatFromMonth($startDate->month, $startDate->year);
            
            $startDate->addMonth();
        }
        
        
        $purchase_types = SellsyID::getPurchaseTypes();
        
        return view('dashboard.view', compact('global_stats', 'datas', 'months_available', 'current_month', 'current_year', 'purchase_types'));
    }
    
    
    public function getStatFromMonth($targetMonth, $targetYear) {
        $this->initSellsy();
        
        $r = [];
        
        $startDate = Carbon::create($targetYear, $targetMonth);
        $endDate = Carbon::create($targetYear, $targetMonth)->endOfMonth();
        
        list($invoicesWithPaymentsThisMonth, $totalPaymentsThisMonth) = $this->sellsy->getInvoicesWithPayments($startDate, $endDate);
        $r['invoicesWithPaymentsThisMonth'] = [
            'list' => $invoicesWithPaymentsThisMonth,
            'total' => $totalPaymentsThisMonth
        ];
        
        $startDate = Carbon::create($targetYear, $targetMonth);
        $endDate = Carbon::create($targetYear, $targetMonth)->endOfMonth();
        
        list($paymentsThisMonth, $totalPaymentsThisMonth) = $this->sellsy->getPayments($startDate, $endDate);
        $r['paymentsThisMonth'] = [
            'list' => $paymentsThisMonth,
            'total' => $totalPaymentsThisMonth
        ];
        
        return $r;
        
    }
    
    private function initSellsy() {
        
        if (!$this->sellsy) {
            $this->sellsy = new SellsyID();;
        }
    }
}