<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Teknoo\Sellsy\Transport\Guzzle;
use Teknoo\Sellsy\Sellsy;
use Carbon\Carbon;


class SellsyID extends Model
{
    const SELLSY_API_URL = 'https://apifeed.sellsy.com/0/';
    
    protected $sellsyClient;
    
    public function __construct() {
        parent::__construct();
        
        $this->initClient();
    }
    
    
    private function initClient() {
        
        
        if (!$this->sellsyClient) {
            $this->setClient();
        }
        
        return $this->sellsyClient;
        
        
    }
    
    private function setClient() {

        //Create the HTTP client
        $guzzleClient = new Client();

        //Create the transport bridge
        $transportBridge = new Guzzle($guzzleClient);

        //Create the front object
        $sellsy = new Sellsy(
            self::SELLSY_API_URL,
            config('sellsy.auth.user_token'),
            config('sellsy.auth.user_secret'),
            config('sellsy.auth.consumer_token'),
            config('sellsy.auth.consumer_secret')
        );

        $sellsy->setTransport($transportBridge);
        $this->sellsyClient = $sellsy;

    }
    
    
    // Methode générique pour appeler des statistiques globales sur une péripde donnée
    public function getGlobalStats($type, $startDate, $endDate) {
        
        if (is_array($type)) {
            $r = [];
            foreach($type as $t) {
                $r[$t] = $this->getGlobalStats($t, $startDate, $endDate);
            }
            
            return $r;
        }
        
        
        $method_name = 'getGlobalStats' . ucfirst($type);
        
        
        return $this->{$method_name}($startDate, $endDate);
    }
    
    
    /**
    Retourne le CA sur la période donnée
    **/
    public function getGlobalStatsCa($startDate, $endDate) {
        
        $params = [
            'search' => [
                'smartDate' => 'last12month',
                'interval' => 'month'
            ]
        ];
        
        // Retourne les stats sur les 12 derniers mois. Il faut donc récupérer la bonne période
        $stats = $this->sellsyClient->Stat()->getSalesStats($params)->getResponse();
        
        $ca = $stats['graph']['line']['ca']['data'];
        
        // on commence par remettre toutes les données organisées par mois / années
        // en sachant que le dernier index retourné correspond au mois / année en cours
        $ca_clean = [];
        $tmp_ca = array_reverse($ca);
        
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        
        foreach($tmp_ca as $ca_month) {
            $tmpDate = Carbon::create($year, $month);
            if (!($tmpDate->gte($startDate) && $tmpDate->lte($endDate))) {
                // on ne garde que les dates dans l'intervalle demandé
                continue;
            }
            
            $ca_clean[$year.'-'.$month] = $ca_month;
            
            $month --;
            if ($month == 0) {
                $month = 12;
                $year --;
            }
        }

        $ca_clean = array_reverse($ca_clean);
        
        return $ca_clean;
    }
    
    
    /**
    Retourne le CA sur la période donnée
    **/
    public function getGlobalStatsChargeDetails($startDate, $endDate) {
        
        $datas = [];
        
        while($startDate->lessThanOrEqualTo($endDate) && $startDate->lessThanOrEqualTo(Carbon::now())) {
            

            $params = [
                'doctype' => 'invoice',
                'search' => [
                    'periodecreated_start' => $startDate->timestamp,
                    'periodecreated_end' => $startDate->toMutable()->addMonth()->timestamp,
                ],
                'pagination' => [
                    'pagenum' => 1,
                    'nbperpage' => 999
                ]
            ];

            // Retourne les stats sur les 12 derniers mois. Il faut donc récupérer la bonne période
            $purchasesRaw = $this->sellsyClient->Purchase()->getList($params)->getResponse();

            $purchases = $purchasesRaw['result'];

            $purchasesDetails= [];
            $codeComptable = 1234;
            $total = 0;
            foreach($purchases as $p) {

                
                $purchaseType = self::codeComptableToPurchaseType($codeComptable);
                
                if (!isset($purchasesDetails[$purchaseType])) {
                    $purchasesDetails[$purchaseType] = 0;
                }
                
                
                $purchasesDetails[$purchaseType] += $p['totalAmount'];
                $total += $p['totalAmount'];
            }
            
            $datas[$startDate->year.'-'.$startDate->month] = [
                'total' => $total,
                'details' => $purchasesDetails
            ];
            
            usleep(250000);
            $startDate->addMonth();

            
        }

        return $datas;
        

        
        
        /*
        $params = [
            'doctype' => 'delivery',
            'search' => [
                'periodecreated_start' => $startDate->timestamp,
                'periodecreated_end' => $endDate->timestamp,
            ],
            'pagination' => [
                'pagenum' => 1,
                'nbperpage' => 999
            ]
        ];
        $purchases = $this->sellsyClient->Document()->getList($params)->getResponse();
        dd($purchases);
        */
        
        /*
        dd($this->sellsyClient->Purchase()->getOne([
            'id' => 1444848,
            'doctype' => 'order'
        ])->getResponse());
        dd($purchases['result']['1444848']);
        */
        /*
        $params = [
            'doctype' => 'delivery',
            'search' => [
                'view' => 'purchase'
            ],
            'pagination' => [
                'pagenum' => 1,
                'nbperpage' => 999
            ]
        ];
        $purchases = $this->sellsyClient->Accounting()->getList($params)->getResponse();
        dd($purchases);*/
    }
    
    
    public function getInvoicesWithPayments($targetStartDate, $targetEndDate) {
        
        $searchStartDate = $targetStartDate->toImmutable()->subMonths(36);
        $searchEndDate = $targetEndDate;
        
        // On récupère les factures créées depuis 6 mois avant le début du mois donné
        $params = [
            'doctype' => 'invoice',
            'includePayments' => true,
            'search' => [
                'periodecreated_start' => $searchStartDate->timestamp,
                'periodecreated_end' => $targetEndDate->timestamp,
            ],
            'pagination' => [
                'pagenum' => 1,
                'nbperpage' => 999
            ]
        ];
        
        
        $invoices = $this->sellsyClient->Document()->getList($params)->getResponse()['result'];
        
        $invoicesList = [];
        $total = 0;
        foreach($invoices as $id_invoice => $invoice) {
            
            // on ne cherche que les factures qui ont eut au moins un paiement ce mois-ci ou plus tard
            if ($invoice['lastpayment'] && Carbon::createFromTimestamp($invoice['lastpayment'])->greaterThanOrEqualTo($targetStartDate)){
                
                $payments = [];
                
                foreach($invoice['payments'] as $p) {
                    $paymentDate = Carbon::createFromTimeString($p['date']);
                    // on check tous les paiements effectués dans la période donnée et on le stocke à part si c'est le cas
                    if ($paymentDate->greaterThanOrEqualTo($targetStartDate) && $paymentDate->lessThanOrEqualTo($targetEndDate)) {
                        $payments[] = $p;
                        $total += $p['amount'];
                    }
                }
                
                if (empty($payments)) {
                    // si finalement il n'y a pas de paiements sur la période donnée, on continue
                    continue;
                }
                
                $invoicesList[] = [
                    'invoice' => $invoice,
                    'paymentsTargeted' => $payments
                ];
                
                
            }
        }
        
        
        return [$invoicesList, $total];
        
    }
    
    public function getPayments($searchStartDate, $searchEndDate) {
        
        // On récupère les factures créées depuis 6 mois avant le début du mois donné
        $params = [
            'search' => [
                'type' => 'credit',
                'start' => $searchStartDate->timestamp,
                'end' => $searchEndDate->sub('1 day')->timestamp, // bug de l'API ...
            ],
            'pagination' => [
                'pagenum' => 1,
                'nbperpage' => 999
            ]
        ];
        
        
        $payments = $this->sellsyClient->Payments()->getList($params)->getResponse()['result'];
        
        $total = 0;
        $paymentsClean = [];
        
        foreach($payments as $p) {
            $paymentsClean[] = $p;
            $total += $p['amount'];
        }
        
        return [$paymentsClean, $total];
    }
    
    public function getPurchases($searchStartDate, $searchEndDate) {
        
        // On récupère les factures créées depuis 6 mois avant le début du mois donné
        $params = [
            'doctype' => 'invoice',
            'search' => [
                'periodecreated_start' => $searchStartDate->timestamp,
                'periodecreated_end' => $searchEndDate->timestamp,
            ]
        ];
        
        
        $payments = $this->sellsyClient->Purchase()->getList($params)->getResponse()['result'];
        
        $total = 0;
        $paymentsClean = [];
        
        foreach($payments as $p) {
            $paymentsClean[] = $p;
            $total += $p['totalAmount'];
        }
        
        return [$paymentsClean, $total];
    }
    
    public static function codeComptableToPurchaseType($codeComptable) {
        
        $purchaseTypes = [
            1234 => 'externe',
            1235 => 'presta',
            1236 => 'personnel'
        ];
        
        if (!isset($purchaseTypes)) {
            throw new ErrorException('Code comptable introuvable : '.$codeComptable);
        }
        
        return $purchaseTypes[$codeComptable];
        
    }
    
    
    public static function getPurchaseTypes() {
        return [
            'externe' => 'charges fixes',
            'presta' => 'prestataires',
            'personnel' => 'frais de personnel',
        ];
    }
}